# The development of this connector is currently on hold

Renku does not currently allow the use of their API without the user being logged in on the Renku user interface. As it goes against the use case of this connector, it's development is being put on hold while the development of Renku progresses to enable third party API utilization.

# This is the Dataverse to Renku connector (WIP)

This React based connector enables data upload between a Dataverse serveur to a Renku server.
Using dataverse External Tools, the connector act as a relay between the two applications.

# Ingress CORS setttings

annotions:
nginx.ingress.kubernetes.io/cors-allow-credentials: "true"
nginx.ingress.kubernetes.io/cors-allow-methods: PUT, GET, POST, OPTIONS
nginx.ingress.kubernetes.io/cors-allow-origin: https://renku-connector.k.orion.cloud.inrae.fr
nginx.ingress.kubernetes.io/enable-cors: "true"
