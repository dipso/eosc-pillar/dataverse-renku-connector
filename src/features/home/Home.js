import React, { useEffect, useState } from 'react'

import { useLocation } from 'react-router'

import { Box, Image, Text, TextInput } from 'grommet'
import GradientText from '../../components/GradientText'
import Card from '../../components/Card'
import ConcaveButton from '../../components/ConcaveButton'

import Lottie from 'react-lottie'

import {importFromDataverse, getFileMetadata, getDatasetMetadata} from '../../tools/importFromDataverse'

import dataverse from '../../assets/images/dataverse.png'
import renku from '../../assets/images/renku.svg'

import { useLocalStorage } from '../../tools/localStorageHook'

const Home = () => {

    const [email, setemail] = useLocalStorage('email', '')
    const [password, setpassword] = useLocalStorage('password', '')
    const [credentials, setcredentials] = useLocalStorage('credentials', '')
    

    const query = new URLSearchParams(useLocation().search)

    const filePid= query.get("filePid") || `dataset`
    const datasetPid= query.get("datasetPid") || `doi:10.70112/V26AIH`

    const uploadfile = async () => {

        const currentToken = await getToken()
        
        const content = await importFromDataverse(filePid)
        //await getUser();
        await uploadDataset()
        const fileUploadEndpoint = process.env.REACT_APP_RENKU_URL + '/api/renku/cache.files_upload'
        await fetch(fileUploadEndpoint, {
            method:'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer '+ currentToken
                
              },
              body : new URLSearchParams(content)
        }).then(
            response => response.json()
        ).then(data => console.log(data))
    }

    const getUser = async () => {
        const currentToken = await getToken()

        const userEndpoint = process.env.REACT_APP_RENKU_URL + '/user'
        await fetch(userEndpoint, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer '+ currentToken
              },
        }).then(
            response => response.json()
        ).then(
            data => console.log(data)
        )
    }

    const getToken = async () => {
        var token = ''
        const tokenEndpoint = process.env.REACT_APP_RENKU_URL + '/auth/realms/Renku/protocol/openid-connect/token'
        const secret = process.env.REACT_APP_CLIENT_SECRET
        const tokenBody = {
            grant_type: "password",
            username: email,
            password:password,
            scope: "openid",
            client_id: "renku",
            client_secret: secret
        }


        await fetch(tokenEndpoint, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
              },
            body : new URLSearchParams(tokenBody)
        })
        .then(response => response.json())
        .then(data => {
            token = data.access_token
        })

        return token

    }

    const uploadDataset = async () => {
//dataset mode 

        const currentToken = await getToken()

        const datasetEndpoint = process.env.REACT_APP_RENKU_URL + '/api/renku/datasets.import'

        const datasetBody = {   
        
            "branch": "master",
            "dataset_uri": "https://data.inrae.fr/dataset.xhtml?persistentId=doi:10.15454/O93984",
            "extract": true,
            "git_url": "string",
            "is_delayed": true,
            "migrate_project": false,
            "name": "test",
            "project_id": "test"
        }

        await fetch(datasetEndpoint, {
            method:'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer '+ currentToken
              },
              body : new URLSearchParams(datasetBody)
        }).then(
            response => response.json()
        ).then(data => console.log(data))

    }

    return (
        <Box align="center">
            <GradientText>Welcome to the EOSC-Pillar Renku Connector </GradientText>

            <Box margin="small">
                <Text>Your dataset : {datasetPid}</Text>
            </Box>
           
            <Box align="center" direction='row'>
                
                <Box width="small">
                   <Image src={dataverse} fit='contain' />
                </Box>
                
                <Lottie 
                options={{
                    loop: true, 
                    path: 'https://assets3.lottiefiles.com/packages/lf20_imnbcwhi.json'
                }}
                height={100}
                />
                
                
                <Box width="small">
                    <Image src={renku} fit='contain' />
                </Box>
         
            </Box>
            {credentials === '' && (
                <Box gap="medium" align='center'>

                    <Text>Please enter your Renku credentials</Text>
                    <Card round='small' pad="small" align='center' justify="center">
                        <Box  align='center' justify="center">
                            <TextInput placeholder="email..."  plain value={email} onChange={event => setemail(event.target.value)} />
                        </Box>
                    </Card>
                    <Card round='small' pad="small" align='center' justify="center">
                        <Box  align='center' justify="center">
                            <TextInput type='password' placeholder="password..."  plain value={password} onChange={event => setpassword(event.target.value)} />
                        </Box>
                    </Card>
                </Box>
             )}

            <Box margin="medium" align='center' justify='center'>
                <ConcaveButton margin="medium" justify='center' align='center' round="small" pad="medium" onClick={()=> {
                        if (email !== '' && password !== '' && filePid !== 'dataset'){
                            uploadfile()
                                
                        }
                    }}>Upload dataset</ConcaveButton>
            </Box>
        </Box>
    )
}

export default Home;