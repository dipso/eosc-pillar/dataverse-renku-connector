
 const dataverseDownloadFileUrl=process.env.REACT_APP_DATAVERSE_URL + "/access/datafile/:persistentId/?persistentId="
 const dataverseBaseUrl=process.env.REACT_APP_DATAVERSE_URL
 const slash_url_escape_value="-"
 const slash="/"
 const two_point=":"
  

export const getFileMetadata = async (filePid) => {

    const complteRessouceUrl = dataverseBaseUrl + "/files/:persistentId/metadata?persistentId="+filePid
    //var data = {}
    var dataverseFileMetadata = {
        fileName : "",
        description : ''
    }
    const response = await fetch(complteRessouceUrl, {method: 'GET'})
    // .then(newdata => data = newdata.json())

    const data = await response.json()
    dataverseFileMetadata.fileName = data['label']

    if (data['description']) {
        dataverseFileMetadata.description = data['description']

    }
    console.log(dataverseFileMetadata)
    return dataverseFileMetadata
}

export const getDatasetMetadata = async (datasetPid) => {
    const complteRessouceUrl = dataverseBaseUrl + "/datasets/:persistentId/?persistentId="+datasetPid
    const response = await fetch(complteRessouceUrl, {method: 'GET'})
    const data = await response.json()
    const fileList = []

    data.data.latestVersion.files.map(file => fileList.push(file.dataFile.persistentId))

    return fileList
}

const dowloadDataverseFile = async (filePid) => {
    const finalDataverseUrl = dataverseDownloadFileUrl + filePid

    var dataToreturn = ''

    await fetch(finalDataverseUrl, {method : 'GET'})
    .then(resp =>resp.blob())
    .then(data => {
        dataToreturn = data
    })
   

    return dataToreturn
}


export const importFromDataverse = async (filePid) => {

    var trueFilePid = filePid
        
    if (filePid.includes(two_point)){
       var  filePidTab = filePid.split(two_point)
        if (filePidTab.length >=1){
            trueFilePid=filePidTab[1]
        }
    }

    // if (datasetPid.includes(two_point)){
    //     var datasetPidTab = datasetPid.split(two_point)
    //     if (datasetPidTab.length >=1){
    //         trueDatasetPid=datasetPidTab[1]
    //     }
    // }

    var filePidWithOutSlash=trueFilePid
	//var datasetPidWithoutSlash=trueDatasetPid

    if (filePidWithOutSlash.includes(slash)){
        filePidWithOutSlash.replace(slash, slash_url_escape_value)
    }

    // if (datasetPidWithoutSlash.includes(slash)){
    //     datasetPidWithoutSlash.replace(slash, slash_url_escape_value)
    // }

   

   const data = await dowloadDataverseFile(filePid)
   
   
 return data
}
